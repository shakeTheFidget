from time import sleep

class QuestList():
	def __init__(self, session):
		self.session = session
		self.quests = []
	
	def __getattr__(self, name):
		if(name == 'minDuration'):
			cur = self.quests[0]
			for q in self.quests[1:]:
				if(q.duration < cur.duration):
					cur = q
			return cur.duration
	
	def getBestByMoney(self, maxDuration = 1200):
		cur = self.quests[0]
		for q in self.quests[1:]:
			if(q.silver/q.duration > cur.silver/cur.duration and q.duration <= maxDuration):
				cur = q
		if(q.duration <= maxDuration):
			return cur
		else:
			return None
	
	def getBestByExperience(self, maxDuration = 1200):
		cur = self.quests[0]
		for q in self.quests[1:]:
			if(q.experience/q.duration > cur.experience/cur.duration and q.duration <= maxDuration):
				cur = q
		if(q.duration <= maxDuration):
			return cur
		else:
			return None
	
	def parse(self, data, result = None):
		if(not result):
			result = data[0:3]
			data = data[3:]
		
		data = data.split('/')
		if(result == '010'):
			for i in range(3):
				quest = Quest(
					self.session,
					number = i+1,
					duration = int(data[241 +i]),
					silver = int(data[283 +i]),
					experience = int(data[280 +i]))
				self.quests.append(quest)

class Quest():
	def __init__(self, session, number, duration = None, silver = None, experience = None):
		self.session = session
		self.number = number
		self.duration = duration
		self.silver = silver
		self.experience = experience
	
	#TODO debug output
	def attend(self):
		try:
			print 'do quest.attend quest'
			r107 = self.session.request('510%i' % (self.number))
			#TODO parse
			print 'done quest.attend quest'
		
			sleep(self.duration)
		
			print 'do quest.request fight'
			r106 = self.session.request('010')
			#TODO parse fight
			print 'done quest.request fight'
		
		except KeyboardInterrupt:
			print 'do quest.abort'
			r108 = self.session.request('511')
			#TODO parse
			print 'done quest.abort'
			return 1
		
		print 'do character.request'
		r004 = self.session.request('004')
		self.session.character.parse(r004)
		print 'done character.request'
		#self.session.character.silver
		return 0
	
	def score(self):
		return float(self.experience) / float(self.duration)
	
	def __repr__(self):
		return '<Quest %i>' % (self.number)

def queryQuests(session):
	data = session.request('010')
	result = data[0:3]
	data = data[3:].split('/')
	quests = []
	for i in range(3):
		quest = Quest(
			session,
			number = i+1,
			duration = int(data[241 +i]),
			silver = int(data[283 +i]),
			experience = int(data[280 +i]))
		quests.append(quest)
	return quests
