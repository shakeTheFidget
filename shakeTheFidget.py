#!/usr/bin/python
from session import Session
from time import time, sleep
from sys import argv


# parse parameters
if(len(argv) < 3):
	print """%s [options, ..] <user> <password>""" % (argv[0])
	exit()
user = argv[-2]
passwd = argv[-1]

# setup session with connection and db
session = Session(user, passwd)
session.connect()
session.dbInit()

# login to the server
session.sfLogin()

# game routine
while(True):
	session.sfQueryTavern()
	if(session.questTime >= session.quests.minDuration):
		quest = session.quests.getBestByExperience()
		if(quest.attend()): break
	
		#if(session.cooldownArena <= 0):
			# find opponent and fight
		#	print 'arena dummy'
	else:
		#TODO CityWard cycle
		break

# destruct session
session.close()








