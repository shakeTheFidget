import socket
from time import sleep
from threading import Thread, Event, Lock

# SQL stuff
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from player import metadata as metadataPlayer

# classes
from player import Player
from quest import QuestList


class Session():
	def __init__(self, user, passwd, host = '213.165.80.92', port = 8005, version = 'v1.31'):
		self.user = user
		self.passwd = passwd
		self.host = host
		self.port = port
		self.version = version
		
		# s&f variables
		self.character = Player(self, user)
		
		self.quests = QuestList(self)
		self.questTime = 0
		
		# interfaces
		self.socket = None
		self.db = None
		self.dbEngine = None
		self.dbSession = None
		
		# locks
		self.lSocket = Lock()
		
		# threads
		self.tNoop = Noop(self)
		self.tQueryList = QueryList(self, self.db)
	
	# function to do cleanup on exit
	def close(self):
		self.disconnect()
		self.dbClose()
	
	# database init and close
	def dbInit(self, filename = None):
		if(not filename): filename = 'sqlite:///%s.db' % (self.user)
		self.dbEngine = create_engine(filename)
		
		#create tables (if !exist)
		metadataPlayer.create_all(self.dbEngine)
		
		#create session
		self.dbSessionMaker = sessionmaker(bind=self.dbEngine)
		self.db = self.dbSessionMaker()
	
	def dbClose(self):
		self.db.close()
	
	# server primitives: connect, send, request, disconnect
	def connect(self, host = None, port = None):
		if(host): self.host = host
		if(port): self.port = port
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((self.host, self.port))
		self.tNoop.start()
	
	def disconnect(self):
		self.tNoop.stop()
		self.tQueryList.stop()
		self.socket.close()
		self.socket = None
	
	def send(self, data):
		with self.lSocket:
			self.socket.send(data.encode('utf8')+chr(0))
	
	def request(self, request, bufferlen = 1024):
		with self.lSocket:
			# discard everything not yet of interest (chat, mail, etc..)
			self.socket.settimeout(0.0)
			try:
				while(True): self.socket.recv(bufflen)
			except:
				self.socket.settimeout(None)
			
			self.socket.send(request.encode('utf8')+chr(0))
			data = ''
			while(data[-1:] != chr(0)):
				data += self.socket.recv(bufferlen)
			return data[:-1].decode('utf8')
	
	# s&f functions
	def sfLogin(self):
		self.request('002%s;%s;%s' % (self.user, self.passwd, self.version))
	
	def sfQueryTavern(self):
		r010 = self.request('010')
		code = r010[0:3]
		data = r010[3:].split('/')

		# questTime remaining
		self.questTime = int(data[456])
		
		# quests
		self.quests.parse(r010)

	def sfWardCity(self, hours = 1):
		if(hours < 1 or hours > 10):
			raise ValueError
		try:
			self.request('502%i' % (int(hours)))
			sleep(hours *3600)
		except KeyboardInterrupt:
			self.request('505')
		finally:
			self.sfQueryTavern()

	
	# getattribute of dynamic values
	def __getattr__(self, name):
		if(name == 'cooldownArena'):
			r010 = self.request('010')
			data = r010.split('/')
			return int(data[460])-int(data[510])

	# representation
	def __repr__(self):
		return '<Session \'%s\'>' % (self.name)




class MyThread(Thread):
	def __init__(self, name = None):
		Thread.__init__(self, name=name)
		self.eStop = Event()
	
	def stop(self):
		if(self.isAlive()):
			self.eStop.set()
			self.join()
		Thread.__init__(self, name=self.name)
	
	def __del__(self):
		self.stop()

class Noop(MyThread):
	def __init__(self, session):
		self.session = session
		MyThread.__init__(self, name='Noop')
	
	def run(self):
		while(not self.eStop.isSet()):
			self.session.send('999')
			self.eStop.wait(50.0)
		self.eStop.clear()

class QueryList(MyThread):
	def __init__(self, session, db):
		self.session = session
		self.db = db
		MyThread.__init__(self)
	
	def run(self):
		playerCount = self.session.request('007;%i' % (100000)).split('/')[-6]
		i = 1
		while(not self.eStop.isSet()):
			data = self.session.request('007;%i' % (i))
			result = data[0:3]
			data = data[3:].split('/')
			for j in range(15):
				try:
					#new = False
					p = self.db.query(Player).filter(Player.name == data[(j *5) +1]).one()
				except NoResultFound, e:
					#new = True
					p = Player(self.session)
					p.name  = data[(j *5) +1]
					self.db.add(p)
				p.honor  = int(data[(j *5) +4])
				p.guild = data[(j *5) +2]
				p.level = abs(int(data[(j *5) +3]))
				#print ('%s%s(%i) of %s - %i' % (['~', '+'][new], p.name, p.level, p.guild, p.honor)).encode('unicode_escape', 'replace')
			self.db.commit()
			i += 15
			self.eStop.wait(5.0)
		self.eStop.clear()
