from sqlalchemy import Column, Integer, String, Text
from sqlalchemy.ext.declarative import declarative_base
from time import sleep
from math import sqrt
import copy

Base = declarative_base()
metadata = Base.metadata

class Player(Base):
	__tablename__ = "Player"
	
	id = Column(Integer, primary_key=True)
	name = Column(String, unique=True)
	guild = Column(String)
	type = Column(Integer)
	level = Column(Integer)
	honor = Column(Integer)
	
	strength = Column(Integer)
	agility = Column(Integer)
	intelligence = Column(Integer)
	stamina = Column(Integer)
	luck = Column(Integer)
	
	def __init__(self, session, name = None):
		self.session = session
		
		self.id = None
		self.name = name
		self.guild = None
		self.type = None
		self.level = None
		self.honor = None
		self.rank = None
		
		self.strength = None
		self.agility = None
		self.intelligence = None
		self.stamina = None
		self.luck = None
	
		#TODO: the attributes me be arranged in an array ??!!
		class attType:
			strength = 1
			agility = 2
			intelligence = 3
			stamina = 4
			luck = 5
		self.attributeType = attType

	def __cmp__(self, other):
		return self.name == other.name
	
	def fight(self):
		print "fight!"
		data = self.session.request('512%s' % (self.name)).split(';')
		honor = int(data[7])
		silver = int(data[8])
		print 'kampf gegen %s: %s(ehre: %s, silber: %s)' % (
			self.name,
			['loose', 'win'][int(data[7]) >= 0],
			honor,
			silver)

	def query(self):
		data = self.session.request('513%s' % (self.name))
		self.parse(data)
	
	def parse(self, data, result = None):
		if(not result):
			result = data[0:3]
			data = data[3:]
		
		if(result == '111' or result == '004'):
			data = data.split('/')
			self.id = int(data[1])
			self.type = int(data[29])
			self.level = int(data[7])
			self.honor = int(data[10])
			self.rank = int(data[11])
			self.strength = int(data[30])+int(data[35])
			self.agility = int(data[31])+int(data[36])
			self.intelligence = int(data[32])+int(data[37])
			self.stamina = int(data[33])+int(data[38])
			self.luck = int(data[34])+int(data[39])
			try: self.guild = data[510].split(';')[2]
			except: pass
			self.min_dmg = int(data[146])
			self.max_dmg = int(data[147])
			self.armor   = int(data[448])
	
	def weapon_dmg(self):
		dmg = float(self.max_dmg + self.min_dmg)/2.0  

		if (dmg > 0):
			return dmg
		else:
			return 1

	def deff_attribute(self,opponent):
		if (opponent.type==1):	# worrier
				return self.strength / 2.0
		if (opponent.type==2):	# mage
				return self.intelligence / 2.0
		if (opponent.type==3): # 
				return self.agility / 2.0
	
	def krit_per(self,opponent):
		per = 1.0 * self.luck * 5 / (opponent.level*2*100)
		if (self.type==1):	# worrier
				maxper = 0.1	# check value
		if (self.type==2):	# mage
				maxper = 0.50
 		if (self.type==3):# 
				maxper = 0.25	# check value
					
		if(per>maxper):per=maxper
		return per
	
	def armor_per(self,opponent):
		per = 0.01 * self.armor/opponent.level
		if (self.type==1):	# worrier
				maxper = 0.5
		if (self.type==2):	# mage
				maxper = 0.1
 		if (self.type==3):# 
				maxper = 0.25
				
		if(per>maxper):per=maxper
		return per
	
	def attack_attribute(self):  
		if (self.type == 1):# worrier
				return self.strength
		if (self.type == 2):# mage
				return self.intelligence
		if (self.type == 3): # 
				return self.agility

	def live(self):  
		if (self.type == 1):# worrier
				return self.stamina*5*(self.level+1)
		if (self.type == 2):# mage
				return self.stamina*2*(self.level+1)
		if (self.type == 3): # 
				return self.stamina*4*(self.level+1)
	
	def averange_damage_to(self,opponent):
		if (self.attack_attribute() < opponent.deff_attribute(self)):
			return 	(1-opponent.armor_per(self)) * ((1-self.krit_per(opponent)) * (self.weapon_dmg()) + self.krit_per(opponent)* (self.weapon_dmg() + self.weapon_dmg()*2*self.attack_attribute()/10) )
	
		return 	(1-opponent.armor_per(self)) * ((1-self.krit_per(opponent)) * (self.weapon_dmg() + self.weapon_dmg()* (self.attack_attribute()-	opponent.deff_attribute(self)) / 10 ) + self.krit_per(opponent)* (self.weapon_dmg() + self.weapon_dmg()*2*self.attack_attribute()/10) )
	
	def strikes_to_kill(self,opponent):
		##the 1st strike does 1x dmg the 2nd 2x dmg the 3rd 3x dmg and so on...
		#opplive = opponent.live()					
		#strike_cnt=0		
		#if (self.averange_damage_to(opponent) > 0): 
		#	while(0<opplive):
		#		opplive -= (1 + 0.5*strike_cnt) * self.averange_damage_to(opponent)
		#		strike_cnt = strike_cnt + 1
		#	return (strike_cnt, opplive)		
		#else:
		#	print "OHOHOH CRAZY damage"	
		#	return 99999		
		return sqrt(4*opponent.live()/self.averange_damage_to(opponent) + 2.25)-2.5 + 1
		
	def strikeDiff(self,opponent):
		A = opponent.strikes_to_kill(self)	
		B = self.strikes_to_kill(opponent)	
		return A-B		
		#return (A[0] - B[0])  , \
		#(A[1] - B[1])/(1.0*B[0]*self.averange_damage_to(opponent)) ,\
		#opponent.live()/self.averange_damage_to(opponent) - self.live()/opponent.averange_damage_to(self) )

	def skillDiff(self,attribType,opponent):	# call with:  aralee.skillDiff(aralee.attributeType.intelligence,aralee)
		# es wird echt zeit typ classen vom player abzuleiten !!!	
		# zumindest des mit den enums sollte man besser loesen 
		newme = copy.deepcopy(self)

		if (attribType == self.attributeType.strength):
				newme.strength += 1
		if (attribType == self.attributeType.agility):
				newme.agility += 1
		if (attribType == self.attributeType.intelligence): 
				newme.intelligence += 1
		if (attribType == self.attributeType.stamina):
				newme.stamina += 1
		if (attribType == self.attributeType.luck):
				newme.luck += 1
				
		return newme.strikeDiff(opponent)-self.strikeDiff(opponent)	#the higher the better ;)
	
	def __repr__(self):
		return '<Player \'%s(%s)\'>' % (
			self.name,
			['', 'wari', 'mage', 'hunt'][self.type or 0])
	










